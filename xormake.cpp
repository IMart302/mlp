#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include "dmatrix.h"


#define N_EXAM 3000

double xxor(double x1, double x2){
    if(x1 == x2){
        return 0.0;
    }
    else{
        return 1.0;
    }
}

int main(){

    DMatrix xordat(N_EXAM, 3);
    std::random_device rd;
    std::mt19937 gen(rd());
    // give "true" 1/4 of the time
    // give "false" 3/4 of the time
    std::bernoulli_distribution d(0.5);
    for(int i = 0; i < N_EXAM; i++){
        xordat.setAt(i, 0, double(d(gen)));
        xordat.setAt(i, 1, double(d(gen)));
        xordat.setAt(i, 2, xxor(xordat.getAt(i, 0), xordat.getAt(i, 1)));
    }

    std::cout << xordat.toString();
    

    return 0;
}