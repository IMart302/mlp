#ifndef MASSERT_H
#define MASSERT_H

#include <string>
#include <cstdlib>
#include <iostream>
class MAssert{
public: 

    static void assert(bool expr, std::string msg){
        if(!expr){
            std::cerr << msg << std::endl;
            abort();
        }
    }
};

#endif


