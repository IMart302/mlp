#ifndef MLP_H
#define MLP_H

#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include "string_utils.h"
#include "dmatrix.h"

double sigmoid(double val){
    return (1.0/(1.0 + exp(-1.0*val)));
}

void vsigmoid(DMatrix& input, DMatrix& output){
    if(input.getRows() != output.getRows() || input.getCols() != output.getCols()){
        output.realloc(input.getRows(), input.getCols());
    }

    for(int i = 0; i < input.getRows(); i++){
        for(int j = 0; j < input.getCols(); j++){
            output.setAt(i, j, sigmoid(input.getAt(i, j)));
        }
    }
}


class OneHotEncoding{
private:
    DMatrix encoder;
    int categories;
public:
    OneHotEncoding(int n_cat){
        this->categories = n_cat;
        encoder.realloc(n_cat, n_cat);
        for(int i = 0; i < n_cat; i++){
            encoder.setAt(i, i, 1.0);
        }
    }

    void cat2vec(int k, DMatrix& v){
        if(k < categories){
            encoder.submatrix(0, encoder.getRows() - 1, k, k, v);
        }
    }

    int vec2cat(DMatrix& v){
        for(int i = 0; i < v.getRows(); i++){
            if(v.getAt(i, 0) == 1.0)
                return i;
        }
        return -1;
    }
};

class Layer{
protected:
    DMatrix weights; // actual weights
    DMatrix bias;    // actual bias
    DMatrix out;     // output of weights*input
    DMatrix act;     // activated output a(out)
    DMatrix error;   // temporal error for each backpropagation
    DMatrix deltaw;  // batch delta weights (1 to m examples)
    DMatrix partialw; // auxiliar for calc the gradient
    DMatrix deltab;  // batch delta bias
    int neurons;
public:
    virtual ~Layer(){

    }

    Layer(){

    }

    Layer(int prev_neurons, int neurons){
        this->neurons = neurons;
        this->weights.realloc(neurons, prev_neurons);
        this->deltaw.realloc(neurons, prev_neurons);
        this->partialw.realloc(neurons, prev_neurons);
        this->out.realloc(neurons, 1);
        this->act.realloc(neurons, 1);
        this->bias.realloc(neurons, 1);
        this->deltab.realloc(neurons, 1);
        this->error.realloc(neurons, 1);
    }

    int getNeurons(){
        return this->neurons;
    }

    void setNeurons(int n){
        this->neurons = n;
    }

    void getWeights(DMatrix& w){
        this->weights.copy(w);
    }

    void getBiases(DMatrix& b){
        this->bias.copy(b);
    }

    void setWeights(DMatrix& w){
        w.copy(this->weights);
    }

    void setBiases(DMatrix& b){
        b.copy(this->bias);
    }

    void randomWeights(){
        this->weights.fillwithRandom(-1.0, 1.0);
        this->bias.fillwithRandom(-1.0, 1.0);
    }

    void propage(DMatrix& input){
        if(input.getRows() != weights.getCols() || input.getCols() != 1){
            std::cout << "vector missmatch \n";
            return;
        }
        //out.fillwith(0);
        //out = weights*input + out;
        //bias.copy(out);
        this->weights.mul(input, this->out);  // out = W*inpunt
        this->out.sum(this->bias);            // out = out + bias = W*input + bias
        //std::cout << "out \n" << out.toString() << std::endl;
        vsigmoid(this->out, this->act);       //f(out)
        //std::cout << "act \n" << act.toString() << std::endl;
    }

    void getOuput(DMatrix& o){
        this->act.copy(o);
    }

    void getErrors(DMatrix& d){
        this->error.copy(d);
    }

    void getWeightedError(DMatrix& d){
        //Wt * d
        //std::cout << "weighted err \n";
        DMatrix wtrans;
        this->weights.transpose(wtrans);
        wtrans.mul(this->error, d);
        //std::cout << "weighted err \n" << d.toString() << std::endl;
    }

    void resetDeltas(){
        this->deltaw.fillwith(0.0);
        this->deltab.fillwith(0.0);
    }

    //http://ufldl.stanford.edu/wiki/index.php/Backpropagation_Algorithm
    void update(double alpha, double lambda, double m){

        //deltaw is destroyed, must be reseted for each batch iteration
        //std::cout<< "W\n" << weights.toString() << std::endl;
        //std::cout <<"PW\n";
        //std::cout << deltaw.toString() << std::endl;
        //Weights update 

        //Delta w have very small numbers. Divided by big number = almost 0 
        //so first multiply by alfa

        this->deltaw.scale(alpha/m);         //DW = (alpha/m)Dw
        //std::cout <<"PW\n";
        //std::cout << deltaw.toString() << std::endl;
        this->deltaw.sum(this->weights, alpha*lambda); //DW = (alpha/m)DW + alpha*lambda*W
     
        this->weights.sum(this->deltaw, -1.0);        //W = W - ((alpha/m)DW + alpha*lamba*W)

        //bias update 
        this->bias.sum(this->deltab, -1.0*(alpha/m));    //b = b - a(1/m deltaB) 
        //std::cout<< "M" << m << std::endl;
    }

    //http://ufldl.stanford.edu/wiki/index.php/Backpropagation_Algorithm
    void updatePartialDeltas(Layer* prev_layer){
        //partial derivatives
        //Here Weigths layer indexes are in preorder, L(i) = { W(i) Out(i) } -> L(i+1) = { W(i+1) Out(i+1) } -> ...
        DMatrix actt;
        prev_layer->getOuput(actt);
        actt.transpose();
        //std::cout << "actt: "<< actt.toString() << std::endl;
        this->error.mul(actt, this->partialw);  //partialw = delta(l+1)*a(l)^T
        //std::cout << "GW \n" << partialw.toString() << std::endl;
        //std::cout << "partial w " <<partialw.getRows()<< " " << partialw.getCols()<< std::endl;
        //std::cout << "w " <<weights.getRows()<<" " << weights.getCols()<< std::endl;
        this->deltaw.sum(this->partialw); //Delta W = Delta W + partial_w J
        //std::cout << "delta w " <<deltaw.getRows()<< " " << deltaw.getCols()<< std::endl;

        this->deltab.sum(this->error);   //Delta b = Delta b + error_l+1 
    }

    virtual void makeErrors(Layer* next_layer){
        std::cout << "Im virtual, never call me \n";
    }
};


class InputLayer : public Layer{
public:
    virtual ~InputLayer(){

    }

    InputLayer():
        Layer()
    {

    }

    void setInput(DMatrix& in){
        in.copy(act);
    }

};

class HiddenLayer : public Layer{
private:
    
public:
    virtual ~HiddenLayer(){

    }

    HiddenLayer(): 
        Layer()
    {

    }

    HiddenLayer(int prev_neurons, int neurons): 
        Layer(prev_neurons, neurons)
    {
 
    }

    virtual void makeErrors(Layer* next_layer){
        //std::cout << "mkhidden ";
        DMatrix derror;
        next_layer->getWeightedError(derror);
        DMatrix temp(this->act.getRows(), 1, 1.0);

        temp.sum(this->act, -1.0);         //temp = (1 - ai)
        temp.elwisemul(this->act);    //temp =  ai(1 - ai)
        derror.elwisemul(temp, this->error); // error = (W)_t*(derror_l+1)*a(1 - a)
    }
};

class OutputLayer : public Layer{
private: 
    DMatrix target;
public:
    virtual ~OutputLayer(){

    }

    OutputLayer() : 
        Layer()
    {

    }

    OutputLayer(int prev_neurons, int neurons): 
        Layer(prev_neurons, neurons)
    {
 
    }

    void setTarget(DMatrix& t){
        t.copy(target);
    }

    virtual void makeErrors(Layer* next_layer){
        //std::cout << "mkoutput ";
        double ok, tk;
        for(int i = 0; i < act.getRows(); i++){
            ok = act.getAt(i, 0);
            tk = target.getAt(i, 0);
            this->error.setAt(i, 0, ok*(1 - ok)*(ok - tk));
        }
        //std::cout << "acti \n" << this->act.toString()<<std::endl;
        //std::cout << "target out \n" << target.toString()<<std::endl;
        //std::cout << "error out \n" << this->error.toString()<<std::endl;
    }
};

class MLP{
private:
    std::vector<HiddenLayer*> hlayers; //why dont use polymorphism? ...
    OutputLayer* olayer;
    InputLayer* ilayer;
    int nmem;
    int input_size;
    int output_size;
public: 
    virtual ~MLP(){
        if(this->olayer != nullptr){
            delete olayer;
        }
        if(this->ilayer != nullptr){
            delete ilayer;
        }
        for(int l = 0; l < this->hlayers.size(); l++){
            if(this->hlayers[l] != nullptr){
                delete this->hlayers[l];
            }
        }
    }

    MLP(int input_size, int output_size){
        this->input_size = input_size;
        this->output_size = output_size;
        this->nmem = this->input_size;
        this->olayer = nullptr;
        this->ilayer = new InputLayer();
    }

    void pushLayer(int neurons){
        hlayers.push_back(new HiddenLayer(nmem, neurons));
        this->nmem = neurons;
    }

    // x(i) , y(i) -> rows 
    void train(DMatrix& examples, DMatrix& targets, double alpha, double lambda, int epocs, std::string& logs_cost){
        
        if(examples.getRows() != targets.getRows()){
            std::cout << "Examples and targets dim not agree\n";
        }
        logs_cost.clear();
        DMatrix lout;
        DMatrix tar;
        DMatrix ttar;
        DMatrix tlout;
        DMatrix infer;
        double cost_epoc;
        if(olayer == nullptr){
            olayer = new OutputLayer(this->nmem, this->output_size);
            olayer->randomWeights();
        }
        //random weights
        for(int l = 0; l < hlayers.size(); l++){
            hlayers[l]->randomWeights();
            //std::cout << "random weights";
        }

        int epoc = 0;
    
        while(epoc < epocs){
            // reset or init partial derivatives

            std::cout << "Training: Epoc -> " << epoc << std::endl;
            for(int l = 0; l < hlayers.size(); l++){
                hlayers[l]->resetDeltas();
            }
            olayer->resetDeltas();

            for(int m = 0; m < examples.getRows(); m++){
                examples.submatrix(m, m, 0, examples.getCols() - 1, lout);
                targets.submatrix(m, m, 0, targets.getCols() - 1, tar);
                //std::cout << "input: \n" << lout.toString() << std::endl;
                //std::cout << "target$: \n" << tar.toString() << std::endl;
                tar.transpose(ttar);
                //tar.normSumOne();
                //std::cout << "$: \n" << tar.toString() << std::endl;
                olayer->setTarget(ttar);
                lout.transpose(tlout);
                //lout.normSumOne();
                ilayer->setInput(tlout);
                //std::cout << "$: \n" << lout.toString() << std::endl;

                //FEEDFORWARD
                if(!hlayers.empty()){
                    for(int l = 0; l < hlayers.size(); l++){
                        hlayers[l]->propage(tlout);
                        hlayers[l]->getOuput(tlout);
                    }
                }
                olayer->propage(tlout);
                //olayer->getOuput(lout);
                //std::cout << "$: \n" << lout.toString() << std::endl;
    
                //MAKE ERRORS
                olayer->makeErrors(nullptr);
                if(!hlayers.empty()){
                    hlayers[hlayers.size()-1]->makeErrors(olayer);
                    for(int l = (hlayers.size() - 2); l >= 0; l--){
                        hlayers[l]->makeErrors(hlayers[l+1]);
                    }
                }

                //MAKE PARTIAL DERIVATIVES
                if(!hlayers.empty()){
                    hlayers[0]->updatePartialDeltas(ilayer);
                    for(int l = 1; l < hlayers.size(); l++){
                        hlayers[l]->updatePartialDeltas(hlayers[l-1]);
                    }
                    olayer->updatePartialDeltas(hlayers[hlayers.size()-1]);
                }
                else{
                    olayer->updatePartialDeltas(ilayer);
                }

                //std::cout << "END PARTIAL DERIVS \n";
            }

            // UPDATE WEIGHTS
            for(int l = 0; l < hlayers.size(); l++){
                //hlayers[l]->update(alpha, lambda, double(examples.getRows()));
                hlayers[l]->update(alpha, lambda, 1.0);
            }
            //olayer->update(alpha, lambda, double(examples.getRows()));
            olayer->update(alpha, lambda, 1.0);

            //calc the cost per epoc
            cost_epoc = 0;
            for(int m = 0; m < examples.getRows(); m++){
                examples.submatrix(m, m, 0, examples.getCols() - 1, lout);
                targets.submatrix(m, m, 0, targets.getCols() - 1, tar);
                tar.transpose(ttar);
                lout.transpose(tlout);

                this->inference(tlout, infer);
                cost_epoc += this->cost(ttar, infer);
            }
            cost_epoc /= (-1.0*double(examples.getRows()));
            logs_cost += (std::to_string(cost_epoc) + "\n");
            epoc++;
        }
    }

    void test(DMatrix& examples, DMatrix& targets, std::string& log){
        OneHotEncoding enc(targets.getCols());
        DMatrix lout;
        DMatrix tar;
        DMatrix out;
        DMatrix confusion(targets.getCols(), targets.getCols(), 0.0);
        int rmx, cmx;
        int truecat;
        long false_pos = 0;
        long true_pos = 0;
        double temp;
        int cats = targets.getCols();
        for(int m = 0; m < examples.getRows(); m++){
            examples.submatrix(m, m, 0, examples.getCols() - 1, lout);
            targets.submatrix(m, m, 0, targets.getCols() - 1, tar);
            
            std::cout << "target: \n"<< tar.toString() << std::endl;
            lout.transpose();
            //lout.normSumOne();
            tar.transpose();
            //tar.normSumOne();
            inference(lout, out);
            std::cout << "infered: \n"<< out.toString() << std::endl;
            //std::cout << std::endl << out.toString() << std::endl;
            //cmx is the predicted cat, truecat is the true cat
            out.max(rmx, cmx);
            
            truecat = enc.vec2cat(tar);
            //std::cout << rmx << std::endl;
            std::cout << "true "<<truecat <<" predicted "<< rmx <<std::endl;
            if(rmx >= 0 && rmx < cats){
                temp = confusion.getAt(truecat, rmx);
                confusion.setAt(truecat, rmx, temp + 1.0);
            }
            
        }

    }

    //cost of a single example
    double cost(DMatrix& target, DMatrix& infered){
        DMatrix ones(target.getRows(), 1, 1.0);
        DMatrix on_yk; //(1 - y_k)
        ones.sum(target, on_yk, -1.0);
        DMatrix on_ht;
        ones.sum(infered, on_ht, -1.0); //(1- ho(x))
        on_ht.mlog();                   // log((1 - ho(x)))

        DMatrix loght;
        infered.mlog(loght);            //log(ho(x))
        
        loght.elwisemul(target);        //yk.*log(ho(x))
        on_ht.elwisemul(on_yk);         //(1 - yk).*log((1 - ho(x)))

        loght.sum(on_ht);               //yk.*log(ho(x)) + (1 - yk).*log((1 - ho(x)))

        return loght.cumsum();
    }

    void inference(DMatrix& in, DMatrix& out){
        in.copy(out);
        if(!hlayers.empty()){
            for(int l = 0; l < hlayers.size(); l++){
                hlayers[l]->propage(out);
                hlayers[l]->getOuput(out);
            }
        }
        olayer->propage(out);
        olayer->getOuput(out);
    }

    void save(std::string name){
        std::ofstream ofs;
        ofs.open(name);

        if(!ofs.is_open()){
            std::cerr << "Cannot open the file\n";
            return;
        }

        ofs << input_size <<"\n";
        ofs << output_size<<"\n";

        DMatrix temp;
        if(!hlayers.empty()){
            for(int l = 0; l < hlayers.size(); l++){
                ofs << "hlayer:"<<hlayers[l]->getNeurons()<<"\n";
                hlayers[l]->getWeights(temp);
                ofs << temp.toString()<<"\n";
                hlayers[l]->getBiases(temp);
                ofs << temp.toString()<<"\n";
            }
        }
        ofs << "olayer:"<<olayer->getNeurons()<<"\n";
        olayer->getWeights(temp);
        ofs << temp.toString()<<"\n";
        olayer->getBiases(temp);
        ofs << temp.toString()<<"\n";
        ofs.close();
    }

    void load(std::string file){
        std::ifstream ifs;
        ifs.open(file);

        if(!ifs.is_open()){
            std::cerr << "Cannot open the file\n";
            return;
        }

        int selec = 0; 
        std::string line;
        std::vector<std::string> toks;
        int wcounter = 0;
        int bcounter = 0;
        int n;
        bool out = false;
        std::string wstr = "";
        std::string bstr = "";
        while(getline(ifs, line) && !out){
            if(selec == 0){
                this->input_size = std::stoi(line);
                this->nmem = this->input_size;
                selec = 1;
                continue;
            }
            if(selec == 1){
                this->output_size = std::stoi(line);
                //std::cout << "asd" << output_size << std::endl;
                selec = 2;
                continue;
            }
            if(selec == 2){
                toks = split(line, ':');
                
                n = std::stoi(toks[1]);
                //std::cout << "asdsdf " << n << std::endl;
                wcounter = 0;
                bcounter = 0;
                if(toks[0] == "hlayer"){
                    selec = 3;
                }
                else if(toks[0] == "olayer"){
                    selec = 4;
                }
                continue;
            }
            if(selec == 3){
                if(wcounter <  n){
                    //std::cout << "asdsdf sdfs " << line << std::endl;
                    if(wcounter == n - 1)
                        wstr += line;
                    else
                        wstr += (line + "\n");
                    wcounter++;
                    continue;
                }
                if(bcounter < n && wcounter >= n){
                    //std::cout << line << std::endl;
                    if(bcounter == n - 1)
                        bstr += line;
                    else
                        bstr += (line + "\n");
                    bcounter++;
                    //continue;
                }
                
                if(bcounter == n && wcounter >= n){
                    //std::cout << wstr << std::endl;
                    //std::cout << bstr << std::endl;
                    DMatrix w;
                    DMatrix b;
                    w.fromString(n, this->nmem, wstr);
                    b.fromString(n, 1, bstr);
                    HiddenLayer* l = new HiddenLayer();
                    l->setWeights(w);
                    l->setBiases(b);
                    l->setNeurons(n);
                    hlayers.push_back(l);
                    wstr.clear();
                    bstr.clear();
                    this->nmem = n;
                    selec = 2;
                }
                continue;
            }
            if(selec == 4){
                if(wcounter <  n){  
                    if(wcounter == n - 1)
                        wstr += line;
                    else
                        wstr += (line + "\n");
                    wcounter++;
                    continue;
                }
                if(bcounter < n && wcounter >= n){
                    //std::cout << line << std::endl;
                    if(bcounter == n - 1)
                        bstr += line;
                    else
                        bstr += (line + "\n");
                    bcounter++;
                    //continue;
                }
                if(bcounter == n && wcounter == n){
                    //std::cout << wstr << std::endl;
                    //std::cout << bstr << std::endl;
                    DMatrix w;
                    DMatrix b;
                    w.fromString(n, this->nmem, wstr);
                    b.fromString(n, 1, bstr);
                    this->olayer = new OutputLayer();
                    this->olayer->setWeights(w);
                    this->olayer->setBiases(b);
                    this->olayer->setNeurons(n);
                    wstr.clear();
                    bstr.clear();
                    out = true;
                }
            }
        }
    }
};

#endif