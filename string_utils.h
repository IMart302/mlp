#ifndef STRING_UTILS_H
#define STRING_UTILS_H
#include <vector>
#include <string>
#include <cctype>


//tokenize string
std::vector<std::string> tokens(std::string str){
  std::string act_token = "";
  std::vector<std::string> vec;
  for(std::string::iterator it = str.begin(); it != str.end(); ++it){
    if(std::isalnum(*it)){
      act_token.push_back(*it);
    }
    else{
      vec.push_back(act_token);
      act_token.clear();
    }
  }
  vec.push_back(act_token);
  return vec;
}

//split string by delim
std::vector<std::string> split(std::string str, char delim){
    std::string act_token = "";
    std::vector<std::string> vec;
    for(std::string::iterator it = str.begin(); it != str.end(); ++it){
        if(*it != delim){
            act_token.push_back(*it);
        }
        else{
            if(!act_token.empty())
                vec.push_back(act_token);
            act_token.clear();
        }
    }
    if(!act_token.empty())
        vec.push_back(act_token);
    
    return vec;
}

#endif