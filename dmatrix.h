#ifndef DMATRIX_H
#define DMATRIX_H

#include <string>
#include <vector>
#include <iostream>
#include <limits>
#include <cmath>
#include "string_utils.h"
#include "mrandom.h"
#include "massert.h"

class DMatrix{
private: 
    double *data;
    int rows, cols;
public:
    DMatrix(){
        data = nullptr;
        rows = cols = 0;
    }

    DMatrix(int rows, int cols){
        this->rows = rows;
        this->cols = cols;
        this->data = new double[rows*cols];
    }

    DMatrix(int rows, int cols, double val): 
        DMatrix(rows, cols)
    {
        for(int i = 0; i < rows*cols; i++){
            this->data[i] = val;
        }
    }

    DMatrix(const DMatrix &m){
        this->rows = m.rows;
        this->cols = m.cols;
        this->data = new double[m.rows*m.cols];
        for(int i = 0; i < m.rows*m.cols; i++){
            this->data[i] = m.data[i];
        }
    }

    virtual ~DMatrix(){
        if(this->data != nullptr){
            delete this->data;
        }
    }

    void realloc(int rows, int cols){
        if(this->data != nullptr){
            delete data;
        }
        this->rows = rows;
        this->cols = cols;
        this->data = new double[rows*cols];
    }

    int getRows(){
        return rows;
    }

    int getCols(){
        return cols;
    }

    void mlog(){
        for(int i = 0; i < this->rows*this->cols; i++){
            this->data[i] = log(this->data[i]);
        }
    }

    void mlog(DMatrix& out){
        if(out.rows != this->rows || out.cols >= this->cols){
            out.realloc(this->rows, this->cols);
        }

        for(int i = 0; i < this->rows*this->cols; i++){
            out.data[i] = log(this->data[i]);
        }
    }

    double cumsum(){
        double s = 0;
        for(int i = 0; i < this->rows*this->cols; i++){
            s += this->data[i];
        }
        return s;
    }

    double getAt(int r, int c){
        if(r >= this->rows || c >= this->cols){
            MAssert::assert(false, "DMatrix::getAt(int r, int c). r or c out of limits");
            return -1;
        }
        return this->data[r*this->cols + c]; 
    }

    void setAt(int r, int c, double v){
        if(r >= this->rows || c >= this->cols){
            MAssert::assert(false, "DMatrix::setAt(int r, int c, double v). r or c out of limits");
            return;
        }
        this->data[r*this->cols + c] = v;
    }

    void normSumOne(){
        double sum = 0;
        for(int i = 0; i < this->rows*this->cols; i++){
            sum += data[i];
        }
        for(int i = 0; i < this->rows*this->cols; i++){
            data[i] /= sum;
        }
    }

    void scale(double scal){
        for(int i = 0; i < this->rows*this->cols; i++){
            this->data[i] = this->data[i]*scal;
        }
    }

    void copy(DMatrix& out){
        if(this->rows != out.rows || this->cols != out.cols){
            out.realloc(this->rows, this->cols);
        }

        for(int i = 0; i<this->rows*this->cols; i++){
            out.data[i] = this->data[i];
        }
    }

    void fillwith(double val){
        for(int i = 0; i < rows*cols; i++){
            data[i] = val;
        }
    }

    void fillwithRandom(double unifa, double unifb){
        MRandomDoubles r(unifa, unifb);
        for(int i = 0; i < this->rows*this->cols; i++){
            this->data[i] = r.getDouble();
        }
    }

    void sum(DMatrix& m, DMatrix& out, double beta = 1.0){
        if(this->rows != m.rows || this->cols != m.cols){
            MAssert::assert(false, "DMatrix::sum(DMatrix& m, DMatrix& out, double beta = 1.0). Dimensions not agree");
            return;
        }

        if(out.rows != this->rows || out.cols != this->cols){
            out.realloc(this->rows, this->cols);
        }

        for(int i = 0; i < this->rows*this->cols; i++){
            out.data[i] = this->data[i] + (beta*m.data[i]);
        }
    }

    void sum(DMatrix& m, double beta = 1.0){
        if(this->rows != m.rows || this->cols != m.cols){
            MAssert::assert(false, "DMatrix::sum(DMatrix& m, double beta = 1.0). Dimensions not agree");
            return;
        }

        for(int i = 0; i < this->rows*this->cols; i++){
            this->data[i] += (beta*m.data[i]);
        }
    }

    void diff(DMatrix& m, DMatrix& out, double beta = 1.0){
        if(this->rows != m.rows || this->cols != m.cols){
            MAssert::assert(false, "DMatrix::diff(DMatrix& m, DMatrix& out, double beta = 1.0). Dimensions not agree");
            return;
        }

        if(out.rows != this->rows || out.cols != this->cols){
            out.realloc(this->rows, this->cols);
        }

        for(int i = 0; i < this->rows*this->cols; i++){
            out.data[i] = this->data[i] - (beta*m.data[i]);
        }
    } 

    void diff(DMatrix& m, double beta = 1.0){
        if(this->rows != m.rows || this->cols != m.cols){
            MAssert::assert(false, "DMatrix::diff(DMatrix& m, double beta = 1.0). Dimensions not agree");
            return;
        }
        for(int i = 0; i < this->rows*this->cols; i++){
            this->data[i] -= (beta*m.data[i]);
        }
    } 

    void mul(DMatrix& m, DMatrix& out){
        if(this->cols != m.rows){
            MAssert::assert(false, "DMatrix::mul(DMatrix& m, DMatrix& out). Dimensions not agree (this cols must be equal to m rows)");
            return;
        }
        if(out.rows != this->rows || out.cols != m.cols){
            out.realloc(this->rows, m.cols);
        }
        out.fillwith(0);
        for(int r = 0; r < out.rows; r++){
            for(int c = 0; c < out.cols; c++){
                for(int k = 0; k < m.rows; k++){
                    out.data[r*m.cols + c] += this->data[r*this->cols + k] * m.data[k*m.cols + c];
                }
            }
        }
    }

    void threshold(double t, double min, double max){
        for(int i = 0; i < this->rows*this->cols; i++){
            if(data[i] >= t){
                data[i] = max;
            }
            else{
                data[i] = min;
            }
        }
    }

    bool compare(DMatrix& m){
        if(this->rows != m.rows || this->cols != m.cols){
            MAssert::assert(false, "DMatrix::compare(DMatrix& m). Dimensions not agree");
            return false;
        }
        for(int i = 0; i < this->rows*this->cols; i++){
            if(this->data[i] != m.data[i]){
                return false;
            }
        }
        return true;
    }

    void mul(DMatrix& m){
        if(this->cols != m.rows){
            MAssert::assert(false, "DMatrix::mul(DMatrix& m). Dimensions not agree (this cols must be equal to m rows)");
            return;
        }

        DMatrix temp;
        this->copy(temp);
        this->realloc(this->rows, m.cols);
        this->fillwith(0);
        for(int r = 0; r < this->rows; r++){
            for(int c = 0; c < this->cols; c++){
                for(int k = 0; k < m.rows; k++){
                    this->data[r*m.cols + c] += temp.data[r*temp.cols + k] * m.data[k*m.cols + c];
                }
            }
        }
    }

    void elwisemul(DMatrix& m, DMatrix& out){
        if(this->rows != m.rows || this->cols != m.cols){
            MAssert::assert(false, "DMatrix::elwisemul(DMatrix& m, DMatrix& out). Dimensions not agree");
            return;
        }
        
        if(out.rows != this->rows || out.cols != m.cols){
            out.realloc(this->rows, m.cols);
        }

        for(int i = 0; i < this->rows*this->cols; i++){
            out.data[i] = this->data[i]*m.data[i];
        }
    }

    void elwisemul(DMatrix& m){
        if(this->rows != m.rows || this->cols != m.cols){
            MAssert::assert(false, "DMatrix::elwisemul(DMatrix& m). Dimensions not agree");
            return;
        }

        for(int i = 0; i < this->rows*this->cols; i++){
            this->data[i] *= m.data[i];
        }
    }

    void submatrix(int rinit, int rend, int cinit, int cend, DMatrix& out){
        if(rend >= this->rows || cend >= this->cols){
            MAssert::assert(false, "DMatrix::submatrix(int rinit, int rend, int cinit, int cend, DMatrix& out). rend or cend out of limits");
        }
        
        int ncols = cend - cinit + 1;
        int nrows = rend - rinit + 1;

        if(out.rows != nrows || out.cols != ncols){
            out.realloc(nrows, ncols);
        }
        
        for(int i = rinit; i < rend + 1; i++){
            for(int j = cinit; j < cend + 1; j++){
                out.setAt(i - rinit, j - cinit, this->getAt(i,j));
            }
        }
    }


    void transpose(){
        double *dd = new double[this->rows*this->cols];

        for(int i = 0; i < this->rows; i++){
            for(int j = 0; j < this->cols; j++){
                dd[j*this->rows+ i] = this->getAt(i,j);
            }
        }

        delete data;
        int temp = this->cols;
        this->cols = this->rows;
        this->rows = temp;
        data = dd;
    }

    void transpose(DMatrix& out){
        if(out.rows != this->cols || out.cols != this->rows){
            out.realloc(this->cols, this->rows);
        }
        for(int i = 0; i < this->rows; i++){
            for(int j = 0; j < this->cols; j++){
                out.setAt(j, i, this->getAt(i, j));
            }
        }
    }

    //DONT USE, HAVE BUG
    //C = aAB + bC
    void gemm(DMatrix& A, DMatrix& B, double alpha, double beta){
        if(A.cols != B.rows || A.rows != this->rows || B.cols != this->cols){
            std::cerr << "Gemm matrix dimension wrong \n";
            return;
        }

        DMatrix m;
        A.mul(B, m);
        for(int i = 0; i < m.rows*m.cols; i++){
            this->data[i] += beta*this->data[i] + alpha*m.data[i];
        }
    }

    //dot product of this mat at column c1 with v mat at column c2
    double dot(int c1, int c2, DMatrix& v){
        if(c1 >= this->cols){
            MAssert::assert(false, "DMatrix::dot(int c1, int c2, DMatrix& v). c1 is out of border of this matrix");
        }
        if(c2 >= v.cols){
            MAssert::assert(false, "DMatrix::dot(int c1, int c2, DMatrix& v). c2 is out of border of v");
        }
        if(this->rows != v.rows){
            MAssert::assert(false, "DMatrix::dot(int c1, int c2, DMatrix& v). Rows dimension not agree, must be equal");
        }
        double sum = 0;
        for(int i = 0; i < v.rows; i++){
            sum += (this->getAt(i, c1)*v.getAt(i, c2));
        }
        return sum;
    }

    double max(int& rmax, int& cmax){
        int imax  = -1;
        double vmax = std::numeric_limits<double>::min();
        for(int i = 0; i < this->rows*this->cols; i++){
            if(data[i] > vmax){
                imax = i;
                vmax = data[i];
            }
        }
        rmax = imax/this->cols;
        cmax = imax%this->cols;
        return vmax;
    }

    std::string toString(){
        std::string s = "";
        for(int i = 0; i < this->rows; i++){
            for(int j = 0; j < this->cols; j++){
                if(j == this->cols - 1){
                    s += (std::to_string(this->getAt(i, j)));    
                }
                else{
                    s += (std::to_string(this->getAt(i, j)) + " ");
                }  
            }
            if(i < this->rows - 1)
                s += "\n";
        }
        return s;
    }

    void fromString(int rows, int cols, std::string& mat){
        this->realloc(rows, cols);
        std::vector<std::string> lines;
        std::vector<std::string> cells;
        lines = split(mat, '\n');
        double val;
      
        for(int l = 0; l < lines.size(); l++){
            cells = split(lines[l], ' ');
            for(int j = 0; j < cells.size(); j++){
                val = std::stod(cells[j]);
                if(l < this->rows && j < this->cols)
                    this->setAt(l, j, val);
            }
        }
    }
};


#endif