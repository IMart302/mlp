#ifndef MRANDOM_H
#define MRANDOM_H

#include <random>

class MRandomDoubles{
private: 
    static std::random_device rd;
    static std::mt19937 gen;
    std::uniform_real_distribution<double> dist;
public:
    explicit MRandomDoubles(double a, double b):
        dist(a, b)
    {

    }
    
    double getDouble(){
        return dist(MRandomDoubles::gen);
    } 
};

std::random_device MRandomDoubles::rd;
std::mt19937 MRandomDoubles::gen(MRandomDoubles::rd());

#endif