#include <iostream>
#include <fstream>
#include <vector>
#include "dmatrix.h"
#include "mlp.h"

#define DATA_ROWS 1000
#define DATA_COLS 3
#define DATA_DIMX 2

#define TRAIN_ROWS 500
#define TEST_ROWS 500


void load_data(DMatrix& data){
    
    data.realloc(DATA_ROWS, DATA_COLS);
    int line_counter = 0;
    std::ifstream ifs;
    ifs.open("xordata.txt");
    std::string line;
    std::string matstring = "";
    while(getline(ifs, line) && line_counter < DATA_ROWS){
        if(line_counter == DATA_ROWS - 1){
            matstring += (line);
        }
        else{
            matstring += (line + "\n");   
        }
        line_counter++;
    }
    data.fromString(DATA_ROWS, DATA_COLS, matstring);
    //std::cout << mnist.toString() <<std::endl;
    //for(int i = 0; i < 100; i++){
    //    for(int j = 780; j < 785; j++){
    //        std::cout << mnist.getAt(i, j) << "  ";
    //    }
    //    std::cout << std::endl;
    //}
    //std::cout << mnist.getRows() << " " << mnist.getCols() << std::endl;   
}

void prepare_data(DMatrix& data, DMatrix& train_examples, DMatrix& train_targets, DMatrix& test_examples, DMatrix& test_targets){
    
    data.submatrix(0, TRAIN_ROWS - 1, 0, DATA_DIMX-1, train_examples);
    data.submatrix(0, TRAIN_ROWS - 1, DATA_DIMX, DATA_DIMX, train_targets);
    
    data.submatrix(TRAIN_ROWS, DATA_ROWS-1, 0, DATA_DIMX-1, test_examples);
    data.submatrix(TRAIN_ROWS, DATA_ROWS-1, DATA_DIMX, DATA_DIMX, test_targets);

    //std::cout << test_examples.toString() << std::endl;
}

void testxor(MLP& xormlp, DMatrix &examples, DMatrix &targets){
    DMatrix lout;
    DMatrix tar;
    DMatrix out;

    int errors = 0;
    int goods = 0;
    for(int m = 0; m < examples.getRows(); m++){
        examples.submatrix(m, m, 0, examples.getCols() - 1, lout);
        targets.submatrix(m, m, 0, targets.getCols() - 1, tar);
        std::cout << "input: \n"<< lout.toString() << std::endl;
        std::cout << "target: \n"<< tar.toString() << std::endl;
        
        lout.transpose();

        tar.transpose();
        //tar.normSumOne();
        xormlp.inference(lout, out);
        std::cout << "infered: \n"<< out.toString() << std::endl;
        out.threshold(0.5, 0.0, 1.0);
        if(tar.compare(out)){
            goods+=1;
        }
        else{
            errors+=1;
        }
        //std::cout << std::endl << out.toString() << std::endl;
        //cmx is the predicted cat, truecat is the true cat
    }
    std::cout << "Errors: " << errors << " Goods: "<< goods << std::endl;
}

int main(){

    DMatrix xordat;
    load_data(xordat);
    std::string lo;

    DMatrix train, target, test, testt;
    prepare_data(xordat, train, target, test, testt);
    MLP mlp(2, 1);
    mlp.load("model_test_xor.txt");

    testxor(mlp, test, testt);

    return 0;
}