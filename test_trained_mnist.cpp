#include <iostream>
#include <fstream>
#include <vector>
#include "dmatrix.h"
#include "mlp.h"

#define MNIST_ROWS 1000
#define MNIST_COLS 785
#define MNIST_DIMX 784

#define TRAIN_ROWS 900
#define TEST_ROWS 100


void load_mnist_data(DMatrix& mnist){
    
    mnist.realloc(MNIST_ROWS, MNIST_COLS);
    int line_counter = 0;
    std::ifstream ifs;
    ifs.open("mnist.txt");
    std::string line;
    std::string matstring = "";
    while(getline(ifs, line) && line_counter < MNIST_ROWS){
        if(line_counter == MNIST_ROWS - 1){
            matstring += (line);
        }
        else{
            matstring += (line + "\n");   
        }
        line_counter++;
    }
    mnist.fromString(MNIST_ROWS, MNIST_COLS, matstring);
}

void prepare_mnist(DMatrix& mnist, DMatrix& train_examples, DMatrix& train_targets, DMatrix& test_examples, DMatrix& test_targets){
    DMatrix ts_tar;
    DMatrix tr_tar;
    
    mnist.submatrix(0, TRAIN_ROWS - 1, 0, MNIST_DIMX-1, train_examples);
    mnist.submatrix(0, TRAIN_ROWS - 1, MNIST_DIMX, MNIST_DIMX, tr_tar);
    
    mnist.submatrix(TRAIN_ROWS, MNIST_ROWS-1, 0, MNIST_DIMX-1, test_examples);
    mnist.submatrix(TRAIN_ROWS, MNIST_ROWS-1, MNIST_DIMX, MNIST_DIMX, ts_tar);

    train_targets.realloc(TRAIN_ROWS, 10);
    train_targets.fillwith(0.0);
    int c;
    for(int i = 0; i < TRAIN_ROWS; i++){
        c = (int)tr_tar.getAt(i, 0);
        train_targets.setAt(i, c, 1.0);
    }

    test_targets.realloc(TEST_ROWS, 10);
    test_targets.fillwith(0.0);
    for(int i = 0; i < TEST_ROWS; i++){
        c = (int)ts_tar.getAt(i, 0);
        test_targets.setAt(i, c, 1.0);
    }
    //std::cout << train_targets.toString();
    //std::cout << train_targets.toString() << std::endl;
}

void testmnist(MLP& mnistmlp, DMatrix& examples, DMatrix& targets){
    DMatrix lout;
    DMatrix tar;
    DMatrix out;

    int rmx, cmx, truev;
    int truecat;
    long false_pos = 0;
    long true_pos = 0;
    double temp;
    int errors = 0, goods = 0;
    for(int m = 0; m < examples.getRows(); m++){
        examples.submatrix(m, m, 0, examples.getCols() - 1, lout);
        targets.submatrix(m, m, 0, targets.getCols() - 1, tar);
        std::cout << "Example "<<m << std::endl;
        std::cout << "target no transp: \n"<< tar.toString() << std::endl;
        lout.transpose();

        tar.transpose();
        tar.max(truev, cmx);
    
        mnistmlp.inference(lout, out);
        std::cout << "infered: \n"<< out.toString() << std::endl;
        
        out.max(rmx, cmx);
        std::cout << "true val: " << truev << " infered: "<<rmx <<std::endl;
        if(rmx == truev){
            goods+=1;
        }
        else{
            errors+=1;
        }
    }
    std::cout << "ERRORS: " << errors << " GOODS: "<< goods << std::endl;
}

int main(int argc, char *argv[]){
    if(argc != 2){
        std::cout << "Need the trained model file as argument\n";
        return -1;
    }

    std::string filename(argv[1]);
    std::ifstream ts;
    ts.open(filename);
    if(!ts.is_open()){
        std::cout << "Cannot open the file (verify the name)\n";
        return -1;
    }
    ts.close();

    DMatrix mnist;
    load_mnist_data(mnist);
    std::string lo;

    DMatrix train, target, test, testt;
    prepare_mnist(mnist, train, target, test, testt);
    MLP mlp(784, 10);
    mlp.load(filename);

    testmnist(mlp, test, testt);
    
    return 0;
}