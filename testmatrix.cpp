#include <iostream>
#include <string>
#include "dmatrix.h"


int main(){

    DMatrix m1(3, 1, 2.0);
    DMatrix m2(2, 3, 2.0);
    DMatrix m3(3, 1);

    m1.fillwithRandom(1,3);
    m2.fillwithRandom(1,3);
    m3.fillwithRandom(1,3);

    std::cout << "M1 \n" <<m1.toString() <<std::endl;
    std::cout << "M2 \n" <<m2.toString() <<std::endl;
    std::cout << "M3 \n" <<m3.toString() <<std::endl;

    m1.mul(m2);

    std::cout << "M1 \n" <<m1.toString() <<std::endl;
    std::cout << "M2 \n" <<m2.toString() <<std::endl;
    std::cout << "M3 \n" <<m3.toString() <<std::endl;


    return 0;
}